import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Application
{

	public static ArrayList<Character> lireFichier(String chemin)
	{
		ArrayList<Character> listeCaractere = new ArrayList<Character>();

		try
		{
			File fichier = new File(chemin);
		    FileReader fichierALire = new FileReader(fichier);
		    int caractere = fichierALire.read();

		    while(caractere != -1)
		    {
		        listeCaractere.add((char)caractere);
		        caractere = fichierALire.read();
		    }

		    fichierALire.close();
		}
		catch(FileNotFoundException e)
		{
		    System.out.println ("Le fichier n'a pas été trouvé "+ e.getMessage());
		}
		catch(IOException e)
		{
		    System.out.println ("Erreur lors de la lecture : " + e.getMessage());
		}

		return listeCaractere;
	}

	public static void main(String[] args)
	{
		if(args.length == 1)
		{
			ArrayList<Character> listeCaracteres = lireFichier(args[0]);
			if(!listeCaracteres.isEmpty())
			{
				System.out.println(Analyseur.analyser(listeCaracteres));
			}
		}
		else System.out.println("Veuillez entrer le nom du fichier à décoder");
	}

}
