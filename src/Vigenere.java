import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

public class Vigenere
{
	public static String decoder(ArrayList<Character> listeCaracteres)
	{
        String texte = "";
        String texteDecrypter = "";

        for(char c : listeCaracteres)
        {
        		char cLower = Character.toLowerCase(c);
        		if ('a' <= cLower && cLower <= 'z')
        		{
        			texte = texte + cLower;
        		}
        }

        String cle = trouverCleAuto(texte);

        //Si la taille de la clé est de une, alors c'est encodé en césar
        if(cle.length() == 1)
        {
        		int decalage = (int)cle.charAt(0)-97;
        		System.out.println("\nIl s'agit d'un texte encodé en César avec une clé de décalage de "+decalage+". Voici la traduction\n");
        		texteDecrypter = Cesar.decoder(listeCaracteres, 26-decalage);
        }
        else
        {
        		System.out.println("\nIl s'agit d'un texte encodé en Vigenere avec une clé "+cle+". Voici la traduction\n");
	        	StringBuffer sb = new StringBuffer(texte);

	    		for(int i = 0; i < texte.length(); i++)//
	    		{
	    			int decalage = (int)cle.charAt(i % cle.length()) - 97;

	    			int lettreCourante = (int)texte.charAt(i);
	    			if(lettreCourante - decalage < 97)
	    				lettreCourante += 26;

	    			int newCharCode = (lettreCourante - 97 - decalage) % 26 + 97;
	    			sb.setCharAt(i, (char)newCharCode);
	    		}
	    		texteDecrypter = sb.toString();
        }

		return texteDecrypter;
    }

	public static String trouverCleAuto(String texte)
	{
		String cle = "";
		try
		{
		// Cassage de la longueur de la clé en calculant l'indice de coincidence
		// On fait en force brute en testant pour chaque longueur de clé possible
		int partition = 0;
		boolean CleNonTrouve = true;
		ArrayList<String> fichiersCrees = new ArrayList<String>();

		// Pour chaque taille de clé possible, mettre jusqu'à la taille totale du texte
		while(CleNonTrouve && partition < texte.length())
		{
			fichiersCrees.clear();
			partition++;
			// Tableau où seront stocké les indices de coincidence
			double ic[] = new double[partition];

			// Pour chaque "partition"
			for(int i = 0; i < partition; i++)
			{
				// Pour une taille de clé, on divise le texte en sous textes qu'on stocke dans des fichiers
				String fichiersGenerees = "vigenere/cle" + partition + "part" + i + ".txt";
				fichiersCrees.add(fichiersGenerees);

				FileWriter out = new FileWriter(fichiersGenerees, false);
				for(int j = 0; j < texte.length(); j++)
				{
					if(j % partition == i)
						out.write(texte.charAt(j));
				}

				out.close(); // On vide le buffer

				// Pour chaque texte créé on stocke l'indice de coincidence calculé
				BufferedReader in = new BufferedReader(new FileReader(fichiersGenerees));
				String sousTexte = in.readLine();

				// Boucle pour calculer la fréquence
				for(int j = 97; j <= 122; j++)
				{
					int frequence = 0;

					for(int k = 0; k < sousTexte.length(); k++)
					{
						if(sousTexte.charAt(k) == (char)j)
							frequence++;
					}

					//Indice de coincidence = le nombre de fois ou la lettre apparait dans le message sur le nombre total de lettres.
					ic[i] += (double) (Math.pow(frequence, 2) / (Math.pow(sousTexte.length(), 2)));
				}

				in.close();
			}

			boolean tailleOK = true;
			System.out.println("Test pour la taille : " + partition);

			for(int i = 0; i < ic.length; i++)
			{
				System.out.println(0.075 + " " + ic[i] + " " + 0.1);
				if(0.1 < ic[i] || 0.075 > ic[i])
					tailleOK = false;
			}

			// Si tous les indices correspondent, c'est certainement la bonne clé ! On la calcule et on la propose
			if(tailleOK)
			{
				System.out.println("La clé est certainement de taille " + partition);
				CleNonTrouve = false;
			}

			// Si la taille de la clé ne parait pas être bonne, on continue
		}

		for(int i = 0; i < fichiersCrees.size(); i++)
		{
			cle += (char)getLettreFrequence(fichiersCrees.get(i));
		}

		}
		catch(Exception e)
		{

		}

		return cle;
	}

	public static int getLettreFrequence(String cheminFichier)
	{
		int decalage = 0;
		try
		{
		// Tableau des fréquences (Code ASCII, Fréquence)
		int frequences[] = new int[26];
		BufferedReader in = new BufferedReader(new FileReader(cheminFichier));
		String message = in.readLine();
		in.close();

		// Calcul des fréquences des lettres
		for(int i = 97; i <= 122; i++)
		{
			for(int j = 0; j < message.length(); j++)
			{
				if(message.charAt(j) == (char)i)
					frequences[i - 97]++;
			}
		}

		int indexMax = 0;
		for(int i = 0; i < 26; i++)
		{
			if(frequences[i] > frequences[indexMax])
				indexMax = i;
		}

		System.out.println("\nLettre la plus fréquente : '" + (char)(indexMax + 97) + "', correspond donc au 'e'");

		// On récupère la valeur du décalage
		decalage = ((int)'z' - (int)'a') - ((((int)'z' - (int)'a') - indexMax) + ((int)'e' - (int)'a'));
		if(decalage < 0)
			decalage += 26;
		System.out.println("Décalage trouvé : " + decalage);

		}
		catch(Exception e)
		{

		}

		return decalage + 97;
	}
}
