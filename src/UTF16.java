import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;

public class UTF16 
{
	public static String decoder(ArrayList<Character> listeCaractere)
	{
		HashMap<Character,Integer> tableHexa = new HashMap<Character,Integer>();
		tableHexa.put('0', 0);
		tableHexa.put('1', 1);
		tableHexa.put('2', 2);
		tableHexa.put('3', 3);
		tableHexa.put('4', 4);
		tableHexa.put('5', 5);
		tableHexa.put('6', 6);
		tableHexa.put('7', 7);
		tableHexa.put('8', 8);
		tableHexa.put('9', 9);
		tableHexa.put('A', 10);
		tableHexa.put('B', 11);
		tableHexa.put('C', 12);
		tableHexa.put('D', 13);
		tableHexa.put('E', 14);
		tableHexa.put('F', 15);


		byte[] octets = new byte[listeCaractere.size()*2];
		int k = 0;

		for(int i=0 ; i<listeCaractere.size() ; i = i + 5)
		{
			int decimal = tableHexa.get(listeCaractere.get(i+2)) * 16 + tableHexa.get(listeCaractere.get(i+3));
			octets[k] = 0;
			octets[k+1] = 0;
			octets[k+2] = 0;
			octets[k+3] = (byte)decimal;
			k = k + 4;
		}
		return new String(octets, StandardCharsets.UTF_16);
	}
}
