# Cryptographie

<div align="center">
<img width="500" height="400" src="Cryptographie.jpg">
</div>

## Description du projet

Application console réalisée avec Java SE en DUT INFO 2 à l'IUT de Nantes dans le cadre du module "Modélisations Mathématiques" durant l'année 2017-2018 avec un groupe de deux personnes. \
Elle consiste à decoder automatiquement des fichiers qui sont encapsulés dans les formats suivants :
- UTF8 ;
- UTF16 ;
- Unicode ;
- Base 64 ;
- Les nombres premiers ;
- Les facteurs premiers ;
- Les chiffres en trop ;
- Vigenere ;
- César.

## Acteurs

### Réalisateurs

Ce projet a été réalisé par un groupe de trois étudiants de l'IUT de Nantes :
- Théo MAHAUDA : theo.mahauda@etu.univ-nantes.fr ;
- Clara DEGREZ : clara.degrez@etu.univ-nantes.fr.

### Encadrants

Ce projet fut encadré par un enseignant de l'IUT de Nantes :
- Simon PLOUFFE : simon.plouffe@univ-nantes.fr.

## Organisation

Ce projet a été agit au sein de l'université de Nantes dans le cadre du module "Modélisations Mathématiques" du DUT INFO 2.

## Date de réalisation

Ce projet a été éxécuté durant l'année 2018 sur la période du module pendant les heures de TP et personnelles à la maison. \
Il a été terminé et rendu le 17/11/2018.

## Technologies, outils et procédés utilisés

Ce projet a été accomplis avec les technologies, outils et procédés suivants :
- Java ;
- Eclipse.
